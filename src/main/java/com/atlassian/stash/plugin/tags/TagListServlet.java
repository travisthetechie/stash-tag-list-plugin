package com.atlassian.stash.plugin.tags;

import com.google.common.base.Charsets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

public class TagListServlet extends HttpServlet
{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        resp.setContentType("text/html");
        resp.setCharacterEncoding(Charsets.UTF_8.name());

        PrintWriter writer = resp.getWriter();
        writer.println("<html><body><h1>Hello HackHouse!</h1></body></html");
    }
}