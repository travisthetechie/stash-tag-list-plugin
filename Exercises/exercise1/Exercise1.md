Add a web-item
==============

In `atlassian-plugin.xml`, you'll see a web-item module that needs to be completed.
For this task, you need to put the web item in the correct section and give it a label and correct link.

*Hint : the web item should go in the menu where you can browse files, commits, branches and pull requests in a repository.*