Now you should have a list of tags on your page, however it doesn't look particularly pretty. It also isn't very
consistent with the rest of Stash (and other Atlassian products for that matter). For that we need to use AUI.


Using AUI
=========

On other repository pages within Stash, a table is used to list things (e.g. list of commits, list of branches). Instead
of using a standard HTML list we can use an [AUI table](https://docs.atlassian.com/aui/latest/docs/tables.html).

We could just hard code the AUI markup into our template. However there are experimental AUI soy templates which we can
use to abstract that away from us. It also gives you the opportunity to explore using soy templates further. The soy
templates are currently poorly documented, so you would be best to look at the [source code](https://bitbucket.org/atlassian/aui/src/master/src/soy/table.soy).

Add the following line to your `soy-templates` `web-resource`:

    <dependency>com.atlassian.auiplugin:aui-experimental-soy-templates</dependency>

This will allow you to call AUI Soy templates within your template. Use the [soy documentation](https://developers.google.com/closure/templates/docs/commands)
to figure out how to call a sub-template.

