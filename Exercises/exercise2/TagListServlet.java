package com.atlassian.stash.plugin.tags;

import com.atlassian.soy.renderer.SoyTemplateRenderer;
import com.atlassian.stash.repository.Repository;
import com.atlassian.stash.repository.RepositoryService;
import com.google.common.base.Charsets;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.Collections;
import java.util.Map;

public class TagListServlet extends HttpServlet
{
    private static final String MODULE_KEY = "com.atlassian.stash.plugin.stash-tag-list-plugin:soy-templates";
    private static final String TEMPLATE_NAME = "plugin.page.tags.page";

    private final RepositoryService repositoryService;
    private final SoyTemplateRenderer soyTemplateRenderer;

    public TagListServlet(RepositoryService repositoryService, SoyTemplateRenderer soyTemplateRenderer)
    {
        this.repositoryService = repositoryService;
        this.soyTemplateRenderer = soyTemplateRenderer;
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException
    {
        RepositoryInfo repositoryInfo = parseRepositoryInfo(req.getPathInfo());
        if (repositoryInfo == null)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        Repository repository = repositoryService.getBySlug(repositoryInfo.getProjectKey(), repositoryInfo.getRepositorySlug());
        if (repository == null)
        {
            resp.sendError(HttpServletResponse.SC_NOT_FOUND);
            return;
        }

        renderResponse(resp, Collections.<String, Object>singletonMap("repository", repository));
    }

    private RepositoryInfo parseRepositoryInfo(String pathInfo)
    {
        // TODO implement me
        return new RepositoryInfo("PROJECT_1", "rep_1");
    }

    private void renderResponse(HttpServletResponse resp, Map<String, Object> data) throws ServletException, IOException
    {
        resp.setContentType("text/html");
        resp.setCharacterEncoding(Charsets.UTF_8.name());

        soyTemplateRenderer.render(resp.getWriter(), MODULE_KEY, TEMPLATE_NAME, data);
    }

    private static class RepositoryInfo
    {

        private final String projectKey;
        private final String repositorySlug;

        public RepositoryInfo(String projectKey, String repositorySlug)
        {
            this.projectKey = projectKey;
            this.repositorySlug = repositorySlug;
        }

        public String getProjectKey()
        {
            return projectKey;
        }

        public String getRepositorySlug()
        {
            return repositorySlug;
        }
    }
}