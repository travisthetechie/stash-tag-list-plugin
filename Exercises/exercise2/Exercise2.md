Congrats! now that you have a web item going, next we're going to render the project name and repository name
Google Closure Templates (more commonly referred to as Soy templates) instead of writing our HTML in the Servlet.


Render Repository Info
======================

Copy the following code to atlassian-plugin.xml:

    <!-- Register a component-imports so we can use them in TagListServlet-->
    <!-- Documentation - https://developer.atlassian.com/docs/getting-started/plugin-modules/component-import-plugin-module -->
    <component-import key="soyTemplateRenderer" interface="com.atlassian.soy.renderer.SoyTemplateRenderer"/>
    <component-import key="repositoryService" interface="com.atlassian.stash.repository.RepositoryService"/>

    <web-resource key="soy-templates">
        <resource name="tag-list.soy" location="static/page/tag-list.soy"/>
    </web-resource>

then, create a `static/page/tag-list.soy` file in the `src/main/resources` directory and paste the following content:

    {namespace plugin.page.tags}


    /**
     * @param repository
     */
    {template .page}
    <html>
        <head>
            <title>Replace me</title>

            // The decorator meta tag is used by Stash to transform this HTML document into one with the navigation
            // and styles for a particular location. This is done using a technology called sitemesh
            // Documentation - https://developer.atlassian.com/stash/docs/latest/reference/plugin-decorators.html
            <meta name="decorator" content="replace.me">
            // Note, you may need extra meta tags which will tell Stash which repository and project the chrome
            // should be for, and what navigation item should be selected. You can find this in the documentation
        </head>
        <body>
            <h1>Hello HackHouse!</h1>
            // Note, Soy has the dollar sign inside the curly braces {$bar} which is different do other templating
            // languages (e.g. velocity) which have the dollar sign outside the braces ${bar}
            <p>You are viewing {$repository.name} in {$repository.project.name}</p>
        </body>
    </html>
    {/template}

Finally, copy the contents of sibling `TagListServlet.java` file provided for this exercise. This class now does
the following:

* Gets the repository info from the url path and do some basic error checking
* Fetch the repository using the info and passed to the soy template to be rendered

But if you read this carefully you'll notice that its missing some info in the url and missing the logic to
extract this! When complete you should be able to navigate from any repository page (including new repositories created)
and view the corresponding page tag

Your task is to get this to work by making changes to `tag-list.soy`, `atlassian-plugin.xml` and
`TagListServlet.java`.